import Phaser from 'phaser';
import Database from 'database-api';

let database = new Database();
let score;

class LeaderBoard extends Phaser.Scene {
  constructor() {
    super('leaderBoardPost');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
    this.scores = [];
  }

  init(data) {
    score = data.score;
  }

  create() {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    const bg = this.add.image(0, 0, 'background').setOrigin(0, 0).setScale(5);

    const title = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 10,
      'LeaderBoard',
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: '#000',
      }
    );

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    const backBtn = this.add.text(
      this.GAME_WIDTH / 2.8,
      this.GAME_HEIGHT / 1.5,
      'Back To Menu',
      {
        fontSize: '32px',
        fill: '#000',
      }
    );

    backBtn.setInteractive();

    backBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = backBtn.x - backBtn.width / 2.6;
      hoverImage.y = backBtn.y + 10;
    });
    backBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    backBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('endGame');
    });

    let element;

    if (window.innerWidth < 800) {
      element = this.add
        .dom(this.GAME_WIDTH / 2 - 67, this.GAME_HEIGHT / 2 - 130)
        .createFromCache('form')
        .setOrigin(0.5, 0.5);
    } else if (window.innerWidth >= 800) {
      element = this.add
        .dom(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2 - 100)
        .createFromCache('form')
        .setOrigin(0.5, 0.5);
    }

    element.addListener('click');
    element.on('click', this.submitScoreHandler);
  }

  submitScoreHandler(event) {
    if (event.target.name === 'loginButton') {
      const username = document.getElementById('username').value;
      const email = document.getElementById('email').value;
      const gameScore = score;
      let scoreData;
      if (email) {
        scoreData = {
          display_name: username,
          email: email,
          score: gameScore,
        };
      } else {
        scoreData = {
          display_name: username,
          score: gameScore,
        };
      }
      database
        .postScoreData(scoreData)
        .then(() => {
          this.scene.start('leaderBoard');
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }

  getScore() {
    console.log('calling db');
    database
      .getLeaderBoard()
      .then((data) => {
        let sortedData = data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        setData(sortedData);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

export default LeaderBoard;
